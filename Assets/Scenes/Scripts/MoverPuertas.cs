using UnityEngine;

public class DoorController : MonoBehaviour
{
    private Quaternion startRot;
    private bool opened = false;
    private Vector3 newRot;
    private float rotateSpeed = 1.0f;

    void Start()
    {
        startRot = transform.rotation;
    }

    void Action()
    {
        // If the door is shut
        if (!opened)
        {
            // While the door's rotation is not equal to the new rotation
            Vector3 myRot = Vector3.zero;
            while (Quaternion.Euler(transform.rotation.eulerAngles) != Quaternion.Euler(newRot))
            {
                // Rotate the door
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(newRot), Time.deltaTime * rotateSpeed);
                float distanceBetween = Quaternion.Angle(transform.rotation, Quaternion.Euler(newRot));
                if (distanceBetween < 0.1f)
                {
                    break;
                }

                // If the object doesn't lerp... for whatever reason
                if (myRot != transform.rotation.eulerAngles)
                {
                    myRot = transform.rotation.eulerAngles;
                }
                else
                {
                    transform.rotation = startRot;
                    break;
                }
            }
            // When while loop ends, opened is true
            opened = true;
        }
        // If the door is open
        else
        {
            // While the door's rotation is not equal to its starting rotation
            Vector3 myRot = Vector3.zero;
            while (Quaternion.Euler(transform.rotation.eulerAngles) != startRot)
            {
                // Rotate the door
                transform.rotation = Quaternion.RotateTowards(transform.rotation, startRot, Time.deltaTime * rotateSpeed);
                float distanceBetween = Quaternion.Angle(transform.rotation, startRot);
                if (distanceBetween < 0.1f)
                {
                    break;
                }
                // If the object doesn't lerp... for whatever reason
                if (myRot != transform.rotation.eulerAngles)
                {
                    myRot = transform.rotation.eulerAngles;
                }
                else
                {
                    transform.rotation = startRot;
                    break;
                }
            }
            // When while loop ends, opened is false
            opened = false;
        }
    }
}