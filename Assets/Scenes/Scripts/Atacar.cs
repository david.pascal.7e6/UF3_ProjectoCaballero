using UnityEngine;

public class Ataque : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("atacar");
        }
    }
}