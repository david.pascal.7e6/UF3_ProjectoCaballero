using UnityEngine;

public class AlternarCamaras : MonoBehaviour
{
    public Camera FirstPerson;
    public Camera TerceraPersona;

    private bool primeraPersonaActiva = true;

    void Start()
    {
      
        TerceraPersona.enabled = false;
    }

    void Update()
    {
        
        if (Input.GetMouseButtonDown(2))
        {
           
            if (primeraPersonaActiva)
            {
                FirstPerson.enabled = false;
                TerceraPersona.enabled = true;
                primeraPersonaActiva = false;
            }
            else
            {
                TerceraPersona.enabled = false;
                FirstPerson.enabled = true;
                primeraPersonaActiva = true;
            }
        }
    }
}