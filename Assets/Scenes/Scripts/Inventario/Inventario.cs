using UnityEngine;
using UnityEngine.UI;

public class ActivarImagenEspada : MonoBehaviour
{
    public GameObject imagenUI; // Referencia a la imagen en el Canvas que quieres activar

    private void OnTriggerEnter(Collider other)
    {
        // Verificamos si el objeto con el que hemos colisionado tiene el tag "Espada"
        if (other.CompareTag("Take"))
        {
            // Activamos la imagen de la UI
            imagenUI.SetActive(true);
        }
    }
}