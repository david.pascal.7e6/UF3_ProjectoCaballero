using UnityEngine.InputSystem;
using UnityEngine;
using System;

public class InputPlayer : MonoBehaviour
{
    private Rigidbody rb;
    private bool isGrounded;
    private Movement movement;
    private float xdeg;
    private Animator animator;
    private AudioSource audioSource;
    public AudioClip movimientoSound;
    public AudioClip saltoSound;


    public static Action PlayerInteracted;

    void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        movement = new Movement();
        animator = this.GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
            audioSource = gameObject.AddComponent<AudioSource>();
    }
    private void OnEnable()
    {
        movement.Enable();
        movement.Player.Jump.started += Jump;
        movement.Player.Interact.performed += _ => PlayerInteracted.Invoke();


    }

    private void LateUpdate()
    {
        float movimientoRatonX = Mouse.current.delta.ReadValue().x;


        transform.Rotate(Vector3.up, movimientoRatonX * 0.2f);
    }

    private void FixedUpdate()
    {
        Move();
    }

    public void Move()
    {
        Vector2 inputVector = movement.Player.Movement.ReadValue<Vector2>();
        if (inputVector != Vector2.zero)
        {
            //rb.velocity = (new Vector3(inputVector.x, rb.velocity.y, inputVector.y) * 10);
            rb.AddRelativeForce(new Vector3(inputVector.x, 0, inputVector.y) * 200);
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, 20);
            animator.SetBool("mover", true);

            if (!audioSource.isPlaying)
                audioSource.PlayOneShot(movimientoSound);
        }
        else animator.SetBool("mover", false);
        if (Input.GetKeyDown(KeyCode.Q))
        {
            animator.SetBool("dance", true);
        }
    }

    private void Jump(InputAction.CallbackContext obj)
    {
        if (isGrounded)
        {
            rb.AddForce(Vector3.up * 10, ForceMode.Impulse);
            isGrounded = false;

            if (!audioSource.isPlaying)
                audioSource.PlayOneShot(saltoSound);
        }
    }



    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Terrain") isGrounded = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Terrain") isGrounded = false;
    }
}
