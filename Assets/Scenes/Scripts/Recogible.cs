using UnityEngine;

public class PlayerPickup : MonoBehaviour
{
    public GameObject objectToDeactivate; // Objeto a desactivar
    public GameObject objectToActivate; // Objeto a activar

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == objectToDeactivate)
        {
            // Desactivar el objeto que toca el personaje
            objectToDeactivate.SetActive(false);

            // Activar el objeto dentro del personaje
            objectToActivate.SetActive(true);
        }
    }
}