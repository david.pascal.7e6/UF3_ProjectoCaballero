using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ControladorDatosJuego : MonoBehaviour
{
    public GameObject jugador;
    public string archivoDeGuardado;
    public DatoJogo datosJuego = new DatoJogo();

    private void Awake()
    {
        archivoDeGuardado = Application.dataPath + "/datosJuego.json";

        jugador = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CargarDatos();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            GuardarPartida();
        }
    }

    private void CargarDatos()
    {
        if (File.Exists(archivoDeGuardado))
        {
            string contenido = File.ReadAllText(archivoDeGuardado);
            datosJuego = JsonUtility.FromJson<DatoJogo>(contenido);

            Debug.Log("Posicion del jugador: " + datosJuego.posicion);

            jugador.transform.position = datosJuego.posicion;
        }
        else
        {
            Debug.Log("No existe");
        }
    }

    private void GuardarPartida()
    {
        DatoJogo NuevosDatos = new DatoJogo()
        {
            posicion = jugador.transform.position
        };

        string cadenaJSON = JsonUtility.ToJson(NuevosDatos);

        File.WriteAllText(archivoDeGuardado, cadenaJSON);

        Debug.Log("Archivo Guardado");
    }
}
